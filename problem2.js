// declare getAllTheItemsContainingOnlyVitaminC function
function getAllTheItemsContainingOnlyVitaminC(arr){

    // if the array length is 0 means it's empty array
    if(arr.length == 0){
        return null;
    }

    // using filter method
    // const result = arr.filter( (obj) => {
    //     // get the current contains vitamins
    //     const currentObjectContains = obj.contains;

    //     // if the current object starts with vitamins C
    //     if(currentObjectContains.startsWith("Vitamin C")){
    //         // means they have only Vitamin C
    //         // so return the current object
    //         return obj;
    //     }
    // });

    // using reduce method
    const result = arr.reduce( (acc, obj) => {
        // get the current contains vitamins
        const currentObjectContains = obj.contains;

        // if the current object starts with vitamins C
        if(currentObjectContains.startsWith("Vitamin C")){
            // means they have only Vitamin C
            // add object in acc array
            acc.push(obj);
        }
        // return the acc to next step
        return acc;
    }, []);

    // return the result
    return result;
}

// export getAllTheItemsContainingOnlyVitaminC function
module.exports = getAllTheItemsContainingOnlyVitaminC;