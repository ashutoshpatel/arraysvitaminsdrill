// import the get getAllTheItemsThatAreAvailable function
const getAllTheItemsThatAreAvailable = require('../problem1.js');
// import items array of object
const items = require('../itemsData.js');

// call the getAllTheItemsThatAreAvailable function
const result = getAllTheItemsThatAreAvailable(items);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the array is empty
    console.log("Array is empty");
}