// import the get sortItemsBasedOnNumberOfVitamins function
const sortItemsBasedOnNumberOfVitamins = require('../problem5.js');
// import items array of object
const items = require('../itemsData.js');

// call the sortItemsBasedOnNumberOfVitamins function
const result = sortItemsBasedOnNumberOfVitamins(items);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the array is empty
    console.log("Array is empty");
}