// import the get getAllTheItemsContainingVitaminA function
const getAllTheItemsContainingVitaminA = require('../problem3.js');
// import items array of object
const items = require('../itemsData.js');

// call the getAllTheItemsContainingVitaminA function
const result = getAllTheItemsContainingVitaminA(items);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the array is empty
    console.log("Array is empty");
}