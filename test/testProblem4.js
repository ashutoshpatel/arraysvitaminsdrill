// import the get groupItemsBasedOnVitamins function
const groupItemsBasedOnVitamins = require('../problem4.js');
// import items array of object
const items = require('../itemsData.js');

// call the groupItemsBasedOnVitamins function
const result = groupItemsBasedOnVitamins(items);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the array is empty
    console.log("Array is empty");
}