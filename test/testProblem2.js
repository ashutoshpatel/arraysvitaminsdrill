// import the get getAllTheItemsContainingOnlyVitaminC function
const getAllTheItemsContainingOnlyVitaminC = require('../problem2.js');
// import items array of object
const items = require('../itemsData.js');

// call the getAllTheItemsContainingOnlyVitaminC function
const result = getAllTheItemsContainingOnlyVitaminC(items);

if(result){
    // print the output
    console.log(result);
}
else{
    // means the array is empty
    console.log("Array is empty");
}