// declare getAllTheItemsContainingVitaminA function
function getAllTheItemsContainingVitaminA(arr){

    // if the array length is 0 means it's empty array
    if(arr.length == 0){
        return null;
    }

    // using filter method
    const result = arr.filter( (obj) => {
        // get the current contains vitamins
        const currentObjectContains = obj.contains;
        // if the current object contains vitamins A
        if(currentObjectContains.includes("Vitamin A")){
            // then return the current object
            return obj;
        }
    });

    // using reduce method
    // const result = arr.reduce( (acc, obj) => {
    //     // get the current contains vitamins
    //     const currentObjectContains = obj.contains;
    //     // if the current object contains vitamins A
    //     if(currentObjectContains.includes("Vitamin A")){{
    //         // add object in acc array
    //         acc.push(obj);
    //     }}
    //     // return the acc to next step
    //     return acc;
    // }, []);

    // return the result
    return result;
}

// export getAllTheItemsContainingVitaminA function
module.exports = getAllTheItemsContainingVitaminA;