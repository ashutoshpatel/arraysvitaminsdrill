// declare getAllTheItemsThatAreAvailable function
function getAllTheItemsThatAreAvailable(arr){

    // if the array length is 0 means it's empty array
    if(arr.length == 0){
        return null;
    }

    // using filter method
    // const result = arr.filter( (obj) => {
    //     if(obj.available){
    //         // then return the current object
    //         return obj;
    //     }
    // });

    // using reduce method
    const result = arr.reduce( (acc, obj) => {
        if(obj.available){{
            // add object in acc array
            acc.push(obj);
        }}
        // return the acc to next step
        return acc;
    }, []);

    // return the result
    return result;
}

// export getAllTheItemsThatAreAvailable function
module.exports = getAllTheItemsThatAreAvailable;