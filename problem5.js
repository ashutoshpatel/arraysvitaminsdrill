// declare sortItemsBasedOnNumberOfVitamins function
function sortItemsBasedOnNumberOfVitamins(arr){

    // if the array length is 0 means it's empty array
    if(arr.length == 0){
        return null;
    }

    // use sort method to sort with some compare function
    const result = arr.sort( (a,b) => {
        // store the a and b object vitamins and create a array
        const containsVitaminsArrayOfAObj = a.contains.split(",");
        const containsVitaminsArrayOfBObj = b.contains.split(",");

        // find which object have more vitamin and which have less vitamin
        const answer = containsVitaminsArrayOfAObj.length - containsVitaminsArrayOfBObj.length;

        return answer;
    });

    // return the result
    return result;
}

// export sortItemsBasedOnNumberOfVitamins function
module.exports = sortItemsBasedOnNumberOfVitamins;