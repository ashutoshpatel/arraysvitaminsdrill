// declare groupItemsBasedOnVitamins function
function groupItemsBasedOnVitamins(arr){

    // if the array length is 0 means it's empty array
    if(arr.length == 0){
        return null;
    }
    
    // using reduce method
    const result = arr.reduce( (acc, obj) => {
        // get the vitamins and create a array containsVitaminArray
        const containsVitaminArray = obj.contains.split(",");

        // use for loop to iterate on containsVitaminArray
        for(let index=0; index < containsVitaminArray.length; index++){
            // get current vitamin and trim so the front and back whitespaces are removed
            const currentVitamin = containsVitaminArray[index].trim();
            // if the acc have already the currentVitamin
            if(acc.hasOwnProperty(currentVitamin)){
                // jush push the fruit name in that key value
                acc[currentVitamin].push(obj.name);
            }
            else{
                // if the currentVitamin is not in acc
                // add object in acc array
                acc[currentVitamin] = [obj.name];
            }
        }
        
        // return the acc to next step
        return acc;
    }, {});

    // return the result
    return result;
}

// export groupItemsBasedOnVitamins function
module.exports = groupItemsBasedOnVitamins;